<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Messaging
</category>

<name>
Zoom
</name>

<description>
   <am>Zoom Teleconference Client</am>
   <ar>Zoom Teleconference Client</ar>
   <be>Zoom Teleconference Client</be>
   <bg>Zoom Teleconference Client</bg>
   <bn>Zoom Teleconference Client</bn>
   <ca>Client de teleconferència Zoom</ca>
   <cs>Zoom Teleconference Client</cs>
   <da>Zoom Teleconference Client</da>
   <de>Clientprogramm für den Videokonferenzdienst “Zoom”</de>
   <el>ζουμ Τηλεδιάσκεψης</el>
   <en>Zoom Teleconference Client</en>
   <es_ES>Cliente de teleconferencia Zoom</es_ES>
   <es>Cliente de teleconferencia Zoom</es>
   <et>Zoom Teleconference Client</et>
   <eu>Zoom Teleconference Client</eu>
   <fa>Zoom Teleconference Client</fa>
   <fil_PH>Zoom Teleconference Client</fil_PH>
   <fi>Zoom etäkonferenssien asiakasohjelma</fi>
   <fr_BE>Client téléconférence Zoom</fr_BE>
   <fr>Client téléconférence Zoom</fr>
   <gl_ES>Zoom Teleconference Client</gl_ES>
   <gu>Zoom Teleconference Client</gu>
   <he_IL>Zoom Teleconference Client</he_IL>
   <hi>ज़ूम टेलीकांफ्रेंस साधन</hi>
   <hr>Zoom Teleconference Client</hr>
   <hu>Zoom Teleconference Client</hu>
   <id>Zoom Teleconference Client</id>
   <is>Zoom Teleconference Client</is>
   <it>Zoom, client per teleconferenze</it>
   <ja>Zoom 電話会議クライアント</ja>
   <kk>Zoom Teleconference Client</kk>
   <ko>Zoom Teleconference Client</ko>
   <ku>Zoom Teleconference Client</ku>
   <lt>Zoom Teleconference Client</lt>
   <mk>Zoom Teleconference Client</mk>
   <mr>Zoom Teleconference Client</mr>
   <nb_NO>Zoom Teleconference Client</nb_NO>
   <nb>Zoom klient for videokonferanse</nb>
   <nl_BE>Zoom Teleconference Client</nl_BE>
   <nl>Zoom Teleconferentie-client</nl>
   <or>Zoom Teleconference Client</or>
   <pl>Zoom Teleconference Client</pl>
   <pt_BR>Zoom cliente de teleconferência (áudio conferência e videoconferência)</pt_BR>
   <pt>Cliente de Videoconferência Zoom</pt>
   <ro>Zoom Teleconference Client</ro>
   <ru>Zoom Teleconference Client</ru>
   <sk>Zoom Teleconference Client</sk>
   <sl>Zoom odjemalec za telekonference</sl>
   <so>Zoom Teleconference Client</so>
   <sq>Klient Zoom Telekonferencash</sq>
   <sr>Zoom Teleconference Client</sr>
   <sv>Zoom Telekonferens-klient</sv>
   <th>Zoom Teleconference Client</th>
   <tr>Zoom Uzaktan Toplantı İstemcisi</tr>
   <uk>Zoom Teleconference Client</uk>
   <vi>Zoom Teleconference Client</vi>
   <zh_CN>Zoom Teleconference Client</zh_CN>
   <zh_HK>Zoom Teleconference Client</zh_HK>
   <zh_TW>Zoom Teleconference Client</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
curl -RL https://zoom.us/client/latest/zoom_amd64.deb -o /tmp/zoom.deb
apt-get install /tmp/zoom.deb
</preinstall>

<install_package_names>

</install_package_names>


<postinstall>
rm /tmp/zoom.deb
</postinstall>


<uninstall_package_names>
zoom
</uninstall_package_names>
</app>

<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Remote Access
</category>

<name>
TeamViewer
</name>

<description>
   <am>Teamviewer remote access</am>
   <ar>Teamviewer remote access</ar>
   <be>Teamviewer remote access</be>
   <bg>Teamviewer remote access</bg>
   <bn>Teamviewer remote access</bn>
   <ca>Accés remot Teamviewer</ca>
   <cs>Teamviewer remote access</cs>
   <da>Teamviewer fjernadgang</da>
   <de>Fernzugriff mit Teamviewer</de>
   <el>Απομακρυσμένη πρόσβαση με το Teamviewer</el>
   <en>Teamviewer remote access</en>
   <es_ES>Acceso remoto de Teamviewer</es_ES>
   <es>Teamviewer acceso remoto</es>
   <et>Teamviewer remote access</et>
   <eu>Teamviewer remote access</eu>
   <fa>Teamviewer remote access</fa>
   <fil_PH>Teamviewer remote access</fil_PH>
   <fi>Teamviewer-etäyhteys</fi>
   <fr_BE>Teamviewer accès à distance</fr_BE>
   <fr>Teamviewer accès à distance</fr>
   <gl_ES>Aplicativo de acceso remoto</gl_ES>
   <gu>Teamviewer remote access</gu>
   <he_IL>Teamviewer remote access</he_IL>
   <hi>Teamviewer दूरस्थ अभिगम</hi>
   <hr>Teamviewer remote access</hr>
   <hu>Teamviewer remote access</hu>
   <id>Teamviewer remote access</id>
   <is>Teamviewer remote access</is>
   <it>Teamviewer accesso remoto</it>
   <ja>TeamViewer リモートアクセス</ja>
   <kk>Teamviewer remote access</kk>
   <ko>Teamviewer remote access</ko>
   <ku>Teamviewer remote access</ku>
   <lt>Teamviewer nuotolinė prieiga</lt>
   <mk>Teamviewer remote access</mk>
   <mr>Teamviewer remote access</mr>
   <nb_NO>Teamviewer remote access</nb_NO>
   <nb>Teamviewer fjerntilgang</nb>
   <nl_BE>Teamviewer remote access</nl_BE>
   <nl>Teamviewer afstand toegang</nl>
   <or>Teamviewer remote access</or>
   <pl>dostęp zdalny TeamViewer</pl>
   <pt_BR>Teamviewer - Aplicativo de acesso remoto</pt_BR>
   <pt>Aplicação de acesso remoto</pt>
   <ro>Teamviewer remote access</ro>
   <ru>Удаленный рабочий стол, передача файлов и видеосвязь</ru>
   <sk>Teamviewer remote access</sk>
   <sl>Teamviewer oddaljeni dostop</sl>
   <so>Teamviewer remote access</so>
   <sq>Hyrje e largët me Teamviewer</sq>
   <sr>Teamviewer remote access</sr>
   <sv>Teamviewer fjärråtkomst</sv>
   <th>การเข้าถึงระยะไกล TeamViewer</th>
   <tr>Teamviewer uzaktan erişim</tr>
   <uk>Teamviewer remote access</uk>
   <vi>Teamviewer remote access</vi>
   <zh_CN>Teamviewer remote access</zh_CN>
   <zh_HK>Teamviewer remote access</zh_HK>
   <zh_TW>Teamviewer remote access</zh_TW>
</description>

<installable>
32,64
</installable>

<screenshot>none</screenshot>

<preinstall>

( curl -RLJ https://download.teamviewer.com/download/linux/signature/TeamViewer2017.asc | apt-key add - ) 2>/dev/null 1>/dev/null
echo "deb http://linux.teamviewer.com/deb stable main" > /etc/apt/sources.list.d/mxtemp.list
apt-get update

</preinstall>

<install_package_names>
teamviewer
</install_package_names>

<postinstall>
rm /etc/apt/sources.list.d/mxtemp.list
apt-get update
</postinstall>

<uninstall_package_names>
teamviewer
</uninstall_package_names>
</app>

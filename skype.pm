<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Messaging
</category>

<name>
Skype (64bit only)
</name>

<description>
   <am>Peer-to-peer video, voice, messaging</am>
   <ar>Peer-to-peer video, voice, messaging</ar>
   <be>Peer-to-peer video, voice, messaging</be>
   <bg>Peer-to-peer video, voice, messaging</bg>
   <bn>Peer-to-peer video, voice, messaging</bn>
   <ca>Missatgeria peer-to-peer de vídeo i veu</ca>
   <cs>Peer-to-peer video, voice, messaging</cs>
   <da>Modpart-til-modpart video, stemme, samtale</da>
   <de>Peer-to-Peer-Video, Sprache, Messaging</de>
   <el>Peer-to-peer βίντεο, φωνή, μηνύματα</el>
   <en>Peer-to-peer video, voice, messaging</en>
   <es_ES>Video, voz y mensajería de igual a igual</es_ES>
   <es>Vídeo entre pares, voz, mensajería</es>
   <et>Peer-to-peer video, voice, messaging</et>
   <eu>Peer-to-peer video, voice, messaging</eu>
   <fa>Peer-to-peer video, voice, messaging</fa>
   <fil_PH>Peer-to-peer video, voice, messaging</fil_PH>
   <fi>Vertaisverkko-video, äänikeskustelu, viestittely</fi>
   <fr_BE>Messagerie vocale, vidéo, peer-to-peer</fr_BE>
   <fr>Messagerie vocale, vidéo, peer-to-peer</fr>
   <gl_ES>Mensaxes, voz e vídeo punto-a-punto</gl_ES>
   <gu>Peer-to-peer video, voice, messaging</gu>
   <he_IL>Peer-to-peer video, voice, messaging</he_IL>
   <hi>सहभाजित नेटवर्क द्वारा वीडियो, ध्वनि व संदेश</hi>
   <hr>Peer-to-peer video, voice, messaging</hr>
   <hu>Peer-to-peer video, voice, messaging</hu>
   <id>Peer-to-peer video, voice, messaging</id>
   <is>Peer-to-peer video, voice, messaging</is>
   <it>Peer-to-peer video, voce e messaggi</it>
   <ja>P2P のビデオ、音声、メッセージ</ja>
   <kk>Peer-to-peer video, voice, messaging</kk>
   <ko>Peer-to-peer video, voice, messaging</ko>
   <ku>Peer-to-peer video, voice, messaging</ku>
   <lt>Peer-to-peer video, voice, messaging</lt>
   <mk>Peer-to-peer video, voice, messaging</mk>
   <mr>Peer-to-peer video, voice, messaging</mr>
   <nb_NO>Peer-to-peer video, voice, messaging</nb_NO>
   <nb>Videotelefoni, taletelefoni og lynmeldinger</nb>
   <nl_BE>Peer-to-peer video, voice, messaging</nl_BE>
   <nl>Peer-to-peer video, voice, berichten</nl>
   <or>Peer-to-peer video, voice, messaging</or>
   <pl>wideo, głos, przesyłanie wiadomości typu peer-to-peer</pl>
   <pt_BR>Mensagens, voz e vídeo ponto-a-ponto</pt_BR>
   <pt>Mensagens, voz e vídeo ponto-a-ponto</pt>
   <ro>Peer-to-peer video, voice, messaging</ro>
   <ru>Голосовая и видеосвязь (IP-телефония)</ru>
   <sk>Peer-to-peer video, voice, messaging</sk>
   <sl>Peer-to-peer video, zvok, sporočila</sl>
   <so>Peer-to-peer video, voice, messaging</so>
   <sq>Shkëmbim mesazhesh video, audio, tek-për-tek</sq>
   <sr>Peer-to-peer video, voice, messaging</sr>
   <sv>Peer-to-peer video, röst, meddelandetjänst</sv>
   <th>Peer-to-peer video, voice, messaging</th>
   <tr>eşler arasında görüntü , ses, ileti</tr>
   <uk>Peer-to-peer video, voice, messaging</uk>
   <vi>Peer-to-peer video, voice, messaging</vi>
   <zh_CN>Peer-to-peer video, voice, messaging</zh_CN>
   <zh_HK>Peer-to-peer video, voice, messaging</zh_HK>
   <zh_TW>對等視頻，語音，消息傳遞</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
wget https://repo.skype.com/latest/skypeforlinux-64.deb
dpkg -i skypeforlinux-64.deb
apt-get -f install
rm skypeforlinux-64.deb
</preinstall>

<install_package_names>

</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
skypeforlinux
</uninstall_package_names>
</app>

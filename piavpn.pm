<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
PIAVPN - Private Internet Access VPN
</name>

<description>
   <am>Private Internet Access VPN</am>
   <ar>Private Internet Access VPN</ar>
   <be>Private Internet Access VPN</be>
   <bg>Private Internet Access VPN</bg>
   <bn>Private Internet Access VPN</bn>
   <ca>Accés privat a internet VPN</ca>
   <cs>Private Internet Access VPN</cs>
   <da>Private Internet Access VPN</da>
   <de>Privater Internetzugang VPN</de>
   <el>Private Internet Access VPN</el>
   <en>Private Internet Access VPN</en>
   <es_ES>Acceso Privado a Internet VPN</es_ES>
   <es>Acceso Privado a Internet (PIA) VPN</es>
   <et>Private Internet Access VPN</et>
   <eu>Private Internet Access VPN</eu>
   <fa>Private Internet Access VPN</fa>
   <fil_PH>Private Internet Access VPN</fil_PH>
   <fi>PIA (Private Internet Access) tuo virtuaalisen VPN-erillisverkon</fi>
   <fr_BE>Accès Internet Privé par VPN</fr_BE>
   <fr>Accès Internet Privé par VPN</fr>
   <gl_ES>Acceso privado ao Internet VPN</gl_ES>
   <gu>Private Internet Access VPN</gu>
   <he_IL>Private Internet Access VPN</he_IL>
   <hi>प्राइवेट इंटरनेट एक्सेस पीआईए</hi>
   <hr>Private Internet Access VPN</hr>
   <hu>Private Internet Access VPN</hu>
   <id>Private Internet Access VPN</id>
   <is>Private Internet Access VPN</is>
   <it>Accesso internet privato VPN</it>
   <ja>プライベートインターネットアクセスVPN</ja>
   <kk>Private Internet Access VPN</kk>
   <ko>Private Internet Access VPN</ko>
   <ku>Private Internet Access VPN</ku>
   <lt>Private Internet Access VPN</lt>
   <mk>Private Internet Access VPN</mk>
   <mr>Private Internet Access VPN</mr>
   <nb_NO>Private Internet Access VPN</nb_NO>
   <nb>Privat internett-tilgang, VPN</nb>
   <nl_BE>Private Internet Access VPN</nl_BE>
   <nl>Privé Internet Toegang VPN</nl>
   <or>Private Internet Access VPN</or>
   <pl>Private Internet Access VPN</pl>
   <pt_BR>Acesso Privado à Internet VPN</pt_BR>
   <pt>Acesso Privado à Internet VPN</pt>
   <ro>Private Internet Access VPN</ro>
   <ru>Частный VPN для выхода в интернет</ru>
   <sk>Private Internet Access VPN</sk>
   <sl>Zasebni spletni dostop preko VPN</sl>
   <so>Private Internet Access VPN</so>
   <sq>Private Internet Access VPN</sq>
   <sr>Private Internet Access VPN</sr>
   <sv>Private Internet Access VPN</sv>
   <th>Private Internet Access VPN</th>
   <tr>Özel İnternet Erişimi VPN</tr>
   <uk>Private Internet Access VPN</uk>
   <vi>Private Internet Access VPN</vi>
   <zh_CN>Private Internet Access VPN</zh_CN>
   <zh_HK>Private Internet Access VPN</zh_HK>
   <zh_TW>Private Internet Access VPN</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
piavpn-downloader-installer
</install_package_names>

<postinstall>

if [ -x /usr/share/pia-downloader-installer/install_piavpn.sh ]; then
/usr/share/pia-downloader-installer/install_piavpn.sh
fi

</postinstall>

<uninstall_package_names>
piavpn-downloader-installer
</uninstall_package_names>
</app>

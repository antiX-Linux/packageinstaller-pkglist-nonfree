<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Audio
</category>

<name>
Spotify
</name>

<description>
   <am>Spotify from Spotify Repo</am>
   <ar>Spotify from Spotify Repo</ar>
   <be>Spotify from Spotify Repo</be>
   <bg>Spotify from Spotify Repo</bg>
   <bn>Spotify from Spotify Repo</bn>
   <ca>Spotify del dipòsit Spotify</ca>
   <cs>Spotify from Spotify Repo</cs>
   <da>Spotify fra Spotify</da>
   <de>Spotify vom Spotify Repo</de>
   <el>Spotify από το Spotify Repo</el>
   <en>Spotify from Spotify Repo</en>
   <es_ES>Spotify de los Repositorios de Spotify</es_ES>
   <es>Spotify de los repositorios de Spotify</es>
   <et>Spotify from Spotify Repo</et>
   <eu>Spotify from Spotify Repo</eu>
   <fa>Spotify from Spotify Repo</fa>
   <fil_PH>Spotify from Spotify Repo</fil_PH>
   <fi>Spotify ohjelmavarastosta Spotify</fi>
   <fr_BE>Spotify du dépôt de Spotify</fr_BE>
   <fr>Spotify du dépôt de Spotify</fr>
   <gl_ES>Spotify do repositorio Spotify Repo</gl_ES>
   <gu>Spotify from Spotify Repo</gu>
   <he_IL>Spotify from Spotify Repo</he_IL>
   <hi>Spotify परीक्षण पैकेज-संग्रह से Spotify</hi>
   <hr>Spotify from Spotify Repo</hr>
   <hu>Spotify from Spotify Repo</hu>
   <id>Spotify from Spotify Repo</id>
   <is>Spotify from Spotify Repo</is>
   <it>Spotify dal repo di Spotify</it>
   <ja>Spotify テストレポの Spotify</ja>
   <kk>Spotify from Spotify Repo</kk>
   <ko>Spotify from Spotify Repo</ko>
   <ku>Spotify from Spotify Repo</ku>
   <lt>Spotify from Spotify Repo</lt>
   <mk>Spotify from Spotify Repo</mk>
   <mr>Spotify from Spotify Repo</mr>
   <nb_NO>Spotify from Spotify Repo</nb_NO>
   <nb>Spotify fra Spotify pakkearkiv</nb>
   <nl_BE>Spotify from Spotify Repo</nl_BE>
   <nl>Spotify uit Spotify Pakketbron</nl>
   <or>Spotify from Spotify Repo</or>
   <pl>Spotify z repozytorium Spotify</pl>
   <pt_BR>Spotify (serviços de streaming de músicas)</pt_BR>
   <pt>Spotify do repositório Spotify Repo</pt>
   <ro>Spotify from Spotify Repo</ro>
   <ru>Сервис потокового аудио включающий более 30 млн. треков</ru>
   <sk>Spotify from Spotify Repo</sk>
   <sl>Spotify iz Spotify repozitorija</sl>
   <so>Spotify from Spotify Repo</so>
   <sq>Spotify prej Depos së të Spotify-it</sq>
   <sr>Spotify from Spotify Repo</sr>
   <sv>Spotify från Spotify Förråd</sv>
   <th>Spotify จาก Spotify Repo</th>
   <tr>Spotify Deposundan Spotify</tr>
   <uk>Spotify from Spotify Repo</uk>
   <vi>Spotify from Spotify Repo</vi>
   <zh_CN>Spotify from Spotify Repo</zh_CN>
   <zh_HK>Spotify from Spotify Repo</zh_HK>
   <zh_TW>Spotify from Spotify Repo</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
curl -sS https://download.spotify.com/debian/pubkey_7A3A762FAFD4A51F.gpg | gpg --dearmor --yes -o /etc/apt/trusted.gpg.d/spotify.gpg
echo "deb http://repository.spotify.com stable non-free">/etc/apt/sources.list.d/spotify.list
apt-get update
</preinstall>

<install_package_names>
spotify-client
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
spotify-client
</uninstall_package_names>

<postuninstall>
rm /etc/apt/trusted.gpg.d/repository-spotify-com-keyring.gpg
</postuninstall>
</app>

<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Games
</category>

<name>
Steam
</name>

<description>
   <am>Steam powered games</am>
   <ar>Steam powered games</ar>
   <be>Steam powered games</be>
   <bg>Steam powered games</bg>
   <bn>Steam powered games</bn>
   <ca>Jocs a través de Steam</ca>
   <cs>Steam powered games</cs>
   <da>Spil fra Steam</da>
   <de>Steam powered Games</de>
   <el>Παιχνίδια του Stream</el>
   <en>Steam powered games</en>
   <es_ES>Juegos potenciados por Steam</es_ES>
   <es>Juegos accionados por Steam</es>
   <et>Steam powered games</et>
   <eu>Steam powered games</eu>
   <fa>Steam powered games</fa>
   <fil_PH>Steam powered games</fil_PH>
   <fi>Steam-pelit</fi>
   <fr_BE>Jeux fonctionnant avec Steam</fr_BE>
   <fr>Jeux fonctionnant avec Steam</fr>
   <gl_ES>Xogos da Plataforma Steam</gl_ES>
   <gu>Steam powered games</gu>
   <he_IL>Steam powered games</he_IL>
   <hi>स्टीम चलित खेल</hi>
   <hr>Steam powered games</hr>
   <hu>Steam powered games</hu>
   <id>Steam powered games</id>
   <is>Steam powered games</is>
   <it>Giochi offerti da Steam</it>
   <ja>Steam で動くゲーム</ja>
   <kk>Steam powered games</kk>
   <ko>Steam powered games</ko>
   <ku>Steam powered games</ku>
   <lt>Steam powered games</lt>
   <mk>Steam powered games</mk>
   <mr>Steam powered games</mr>
   <nb_NO>Steam powered games</nb_NO>
   <nb>Steam-baserte spill</nb>
   <nl_BE>Steam powered games</nl_BE>
   <nl>Steam aangedreven spellen</nl>
   <or>Steam powered games</or>
   <pl>Gry na Steam</pl>
   <pt_BR>Jogos da Plataforma Steam</pt_BR>
   <pt>Jogos da Plataforma Steam</pt>
   <ro>Steam powered games</ro>
   <ru>Платформа цифрового распространения игр</ru>
   <sk>Steam powered games</sk>
   <sl>Steam igre</sl>
   <so>Steam powered games</so>
   <sq>Lojëra të bazuara në Steam</sq>
   <sr>Steam powered games</sr>
   <sv>Steam-drivna spel</sv>
   <th>Steam powered games</th>
   <tr>Steam detekli oyunlar</tr>
   <uk>Steam powered games</uk>
   <vi>Steam powered games</vi>
   <zh_CN>Steam powered games</zh_CN>
   <zh_HK>Steam powered games</zh_HK>
   <zh_TW>Steam powered games</zh_TW>
</description>

<installable>
32,64
</installable>

<screenshot>https://screenshots.debian.net/shrine/screenshot/14991/simage/large-fdb68e27705c0554fa7ffc10e4ba1dc0.png</screenshot>

<preinstall>

</preinstall>

<install_package_names>
steam-installer
steam-libs
steam-libs-i386
</install_package_names>


<postinstall>
apt-get install -f
</postinstall>


<uninstall_package_names>
steam-installer
steam-libs
steam-libs-i386
</uninstall_package_names>
</app>

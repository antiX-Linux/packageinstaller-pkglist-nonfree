<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Messaging
</category>

<name>
MS-Teams
</name>

<description>
   <am>Microsoft Teams</am>
   <ar>Microsoft Teams</ar>
   <be>Microsoft Teams</be>
   <bg>Microsoft Teams</bg>
   <bn>Microsoft Teams</bn>
   <ca>Microsoft Teams</ca>
   <cs>Microsoft Teams</cs>
   <da>Microsoft Teams</da>
   <de>“Microsoft Teams”</de>
   <el>Microsoft Teams</el>
   <en>Microsoft Teams</en>
   <es_ES>Equipos de Microsoft</es_ES>
   <es>Microsoft Teams</es>
   <et>Microsoft Teams</et>
   <eu>Microsoft Teams</eu>
   <fa>Microsoft Teams</fa>
   <fil_PH>Microsoft Teams</fil_PH>
   <fi>Microsoft Teams</fi>
   <fr_BE>Microsoft Teams</fr_BE>
   <fr>Microsoft Teams</fr>
   <gl_ES>Microsoft Teams</gl_ES>
   <gu>Microsoft Teams</gu>
   <he_IL>Microsoft Teams</he_IL>
   <hi>माइक्रोसॉफ्ट टीम</hi>
   <hr>Microsoft Teams</hr>
   <hu>Microsoft Teams</hu>
   <id>Microsoft Teams</id>
   <is>Microsoft Teams</is>
   <it>Microsoft Teams</it>
   <ja>マイクロソフト・チーム</ja>
   <kk>Microsoft Teams</kk>
   <ko>Microsoft Teams</ko>
   <ku>Microsoft Teams</ku>
   <lt>Microsoft Teams</lt>
   <mk>Microsoft Teams</mk>
   <mr>Microsoft Teams</mr>
   <nb_NO>Microsoft Teams</nb_NO>
   <nb>Microsoft Teams</nb>
   <nl_BE>Microsoft Teams</nl_BE>
   <nl>Microsoft Teams</nl>
   <or>Microsoft Teams</or>
   <pl>Microsoft Teams</pl>
   <pt_BR>Microsoft Teams</pt_BR>
   <pt>Microsoft Teams</pt>
   <ro>Microsoft Teams</ro>
   <ru>Microsoft Teams</ru>
   <sk>Microsoft Teams</sk>
   <sl>Microsoft Teams</sl>
   <so>Microsoft Teams</so>
   <sq>Microsoft Teams</sq>
   <sr>Microsoft Teams</sr>
   <sv>Microsoft Teams</sv>
   <th>Microsoft Teams</th>
   <tr>Microsoft Ekipleri</tr>
   <uk>Microsoft Teams</uk>
   <vi>Microsoft Teams</vi>
   <zh_CN>Microsoft Teams</zh_CN>
   <zh_HK>Microsoft Teams</zh_HK>
   <zh_TW>Microsoft Teams</zh_TW>
</description>

<installable>
64
</installable>

<screenshot></screenshot>

<preinstall>
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EB3E94ADBE1229CF
echo "deb [arch=amd64] https://packages.microsoft.com/repos/ms-teams stable main">/etc/apt/sources.list.d/mxpiteams.list
apt-get update
</preinstall>

<install_package_names>
apt-transport-https
teams
</install_package_names>


<postinstall>
rm /etc/apt/sources.list.d/mxpiteams.list
</postinstall>


<uninstall_package_names>
teams
</uninstall_package_names>
</app>

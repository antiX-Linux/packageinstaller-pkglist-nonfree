<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Flash
</category>

<name>
adobe flash
</name>

<description>
   <am>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</am>
   <ar>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</ar>
   <be>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</be>
   <bg>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</bg>
   <bn>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</bn>
   <ca>Adobe Flash oficial, incloent-hi extensions per Chromium (pepperflash) i Firefox</ca>
   <cs>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</cs>
   <da>Officiel Adobe Flash, inklusiv plugins til chromium (pepperflash) og firefox</da>
   <de>Offizielles Adobe-Flash-Plugin für Chromium (pepperflash) und Firefox</de>
   <el>Το επίσημο Adobe Flash, συμπεριλαμβανομένων των πρόσθετων για το chromium (pepperflash) και το firefox</el>
   <en>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</en>
   <es_ES>Adobe Flash oficial. Incluye plugins para Chromium (pepperflash) y Firefox</es_ES>
   <es>Adobe Flash oficial. Incluye plugins para Chromium (pepperflash) y Firefox</es>
   <et>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</et>
   <eu>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</eu>
   <fa>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</fa>
   <fil_PH>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</fil_PH>
   <fi>Virallinen Adobe Flash, sisältäen liitännäiset Chromium:ille (pepperflash) sekä Firefox:ille</fi>
   <fr_BE>Adobe Flash officiel, incluant plugins pour Chromium (pepperflash) et Firefox</fr_BE>
   <fr>Adobe Flash officiel, incluant plugins pour Chromium (pepperflash) et Firefox</fr>
   <gl_ES>Flash oficial da Adobe que inclúe suplementos (plugins) para chromium (pepperflash) e firefox</gl_ES>
   <gu>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</gu>
   <he_IL>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</he_IL>
   <hi>आधिकारिक एडोबी फ़्लैश, क्रोमियम (पैपरफ़्लैश) व फायरफॉक्स हेतु प्लगिन युक्त</hi>
   <hr>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</hr>
   <hu>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</hu>
   <id>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</id>
   <is>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</is>
   <it>Adobe Flash ufficiale, contenente plugins per chromium (pepperflash) e firefox</it>
   <ja>Chromium (Pepperflash)とFirefox用のプラグインを含む、公式の Adobe Flash</ja>
   <kk>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</kk>
   <ko>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</ko>
   <ku>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</ku>
   <lt>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</lt>
   <mk>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</mk>
   <mr>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</mr>
   <nb_NO>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</nb_NO>
   <nb>Offisiell Adobe Flash, inkludert tillegg for Chromium (pepperflash) og Firefox</nb>
   <nl_BE>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</nl_BE>
   <nl>Officiële Adobe Flash, inclusief plugins voor chromium (pepperflash) en firefox</nl>
   <or>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</or>
   <pl>oficjalny Adobe Flash, w tym wtyczki do Chromium (Pepperflash) i Firefoksa</pl>
   <pt_BR>Adobe Flash oficial que inclui suplementos (plugins) para o Chromium (pepperflash) e para o Firefox</pt_BR>
   <pt>Flash oficial da Adobe que inclui suplementos (plugins) para chromium (pepperflash) e firefox</pt>
   <ro>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</ro>
   <ru>Официальный Adobe Flash, включающий плагины для Chromium (pepperflash) и Firefox</ru>
   <sk>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</sk>
   <sl>Uradni Adobe Flash, skupaj z vstavki za chromium (pepperflash) in firefox</sl>
   <so>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</so>
   <sq>Adobe Flash zyrtar, përfshi shtojca për Chromium (pepperflash) dhe Firefox</sq>
   <sr>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</sr>
   <sv>Officiell Adobe Flash, inkluderande plugins för chromium (pepperflash) och firefox</sv>
   <th>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</th>
   <tr>chromium (pepperflash) ve firefox eklentilerini içeren resmi Adobe Flash</tr>
   <uk>Офіційний Adobe Flash, включно з плагінами для chromium (pepperflash) та firefox</uk>
   <vi>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</vi>
   <zh_CN>官方版 Adobe Flash，包含 Chromium（pepperflash）和 Firefox 的插件</zh_CN>
   <zh_HK>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</zh_HK>
   <zh_TW>Official Adobe Flash, including plugins for chromium (pepperflash) and firefox</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
adobe-flashplugin
adobe-flash-properties-gtk
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
adobe-flashplugin
adobe-flash-properties-gtk
</uninstall_package_names>
</app>

<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Development
</category>

<name>
MS-VSCode
</name>

<description>
   <am>Microsoft Visual Studio Code</am>
   <ar>Microsoft Visual Studio Code</ar>
   <be>Microsoft Visual Studio Code</be>
   <bg>Microsoft Visual Studio Code</bg>
   <bn>Microsoft Visual Studio Code</bn>
   <ca>Microsoft Visual Studio Code</ca>
   <cs>Microsoft Visual Studio Code</cs>
   <da>Microsoft Visual Studio Code</da>
   <de>“Microsoft Visual Studio Code”</de>
   <el>Microsoft Visual Studio Code</el>
   <en>Microsoft Visual Studio Code</en>
   <es_ES>Microsoft Visual Studio Code</es_ES>
   <es>Microsoft Visual Studio Code</es>
   <et>Microsoft Visual Studio Code</et>
   <eu>Microsoft Visual Studio Code</eu>
   <fa>Microsoft Visual Studio Code</fa>
   <fil_PH>Microsoft Visual Studio Code</fil_PH>
   <fi>Microsoft Visual Studio Code</fi>
   <fr_BE>Microsoft Visual Studio Code</fr_BE>
   <fr>Microsoft Visual Studio Code</fr>
   <gl_ES>Microsoft Visual Studio Code</gl_ES>
   <gu>Microsoft Visual Studio Code</gu>
   <he_IL>Microsoft Visual Studio Code</he_IL>
   <hi>माइक्रोसॉफ्ट विज़ुअल स्टूडियो कोड</hi>
   <hr>Microsoft Visual Studio Code</hr>
   <hu>Microsoft Visual Studio Code</hu>
   <id>Microsoft Visual Studio Code</id>
   <is>Microsoft Visual Studio Code</is>
   <it>Microsoft Visual Studio Code</it>
   <ja>マイクロソフト・ビジュアルスタジオコード</ja>
   <kk>Microsoft Visual Studio Code</kk>
   <ko>Microsoft Visual Studio Code</ko>
   <ku>Microsoft Visual Studio Code</ku>
   <lt>Microsoft Visual Studio Code</lt>
   <mk>Microsoft Visual Studio Code</mk>
   <mr>Microsoft Visual Studio Code</mr>
   <nb_NO>Microsoft Visual Studio Code</nb_NO>
   <nb>Microsoft Visual Studio Code</nb>
   <nl_BE>Microsoft Visual Studio Code</nl_BE>
   <nl>Microsoft Visual Studio Code</nl>
   <or>Microsoft Visual Studio Code</or>
   <pl>Microsoft Visual Studio Code</pl>
   <pt_BR>Microsoft Visual Studio Code</pt_BR>
   <pt>Microsoft Visual Studio Code</pt>
   <ro>Microsoft Visual Studio Code</ro>
   <ru>Microsoft Visual Studio Code</ru>
   <sk>Microsoft Visual Studio Code</sk>
   <sl>Microsoft Visual Studio Code</sl>
   <so>Microsoft Visual Studio Code</so>
   <sq>Microsoft Visual Studio Code</sq>
   <sr>Microsoft Visual Studio Code</sr>
   <sv>Microsoft Visual Studio Kod</sv>
   <th>Microsoft Visual Studio Code</th>
   <tr>Microsoft Visual Studio Kodu</tr>
   <uk>Microsoft Visual Studio Code</uk>
   <vi>Microsoft Visual Studio Code</vi>
   <zh_CN>Microsoft Visual Studio Code</zh_CN>
   <zh_HK>Microsoft Visual Studio Code</zh_HK>
   <zh_TW>Microsoft Visual Studio Code</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>https://code.visualstudio.com/assets/home/home-screenshot-linux-lg.png</screenshot>

<preinstall>
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/microsoft.gpg
install -o root -g root -m 644 /tmp/microsoft.gpg /etc/apt/trusted.gpg.d/
echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main">/etc/apt/sources.list.d/vscode.list
apt-get update
</preinstall>

<install_package_names>
apt-transport-https
code
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
code
</uninstall_package_names>
</app>

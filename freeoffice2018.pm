<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Office
</category>

<name>
SoftMaker FreeOffice 2018
</name>

<description>
   <am>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</am>
   <ar>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</ar>
   <be>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</be>
   <bg>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</bg>
   <bn>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</bn>
   <ca>SoftMaker FreeOffice 2018 és una suite ofimàtica ràpida, compatible i fiable.</ca>
   <cs>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</cs>
   <da>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</da>
   <de>SoftMaker FreeOffice 2018 ist ein schnelles, kompatibles und zuverlässiges Paket der Standardbüroprogramme</de>
   <el>Το SoftMaker FreeOffice 2018 είναι μια γρήγορη, συμβατή και αξιόπιστη σουίτα γραφείου</el>
   <en>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</en>
   <es_ES>SoftMaker FreeOffice 2018 es una suite ofimática rápida, compatible y confiable</es_ES>
   <es>SoftMaker FreeOffice 2018 es una suite ofimática rápida, compatible y confiable</es>
   <et>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</et>
   <eu>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</eu>
   <fa>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</fa>
   <fil_PH>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</fil_PH>
   <fi>SoftMaker:in FreeOffice 2018 on nopea, yhteensopiva ja luotettava toimisto-ohjelmistokokonaisuus</fi>
   <fr_BE>SoftMaker FreeOffice 2018 est une suite bureautique fiable, rapide et compatible</fr_BE>
   <fr>SoftMaker FreeOffice 2018 est une suite bureautique fiable, rapide et compatible</fr>
   <gl_ES>O SoftMaker FreeOffice 2018 é unha suite de escritorio rápida, compatible con outras e fiable</gl_ES>
   <gu>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</gu>
   <he_IL>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</he_IL>
   <hi>सॉफ्टमेकर फ्री ऑफिस 2018 - एक तीव्र, संगत व विश्वसनीय ऑफिस अनुप्रयोग संग्रह</hi>
   <hr>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</hr>
   <hu>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</hu>
   <id>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</id>
   <is>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</is>
   <it>SoftMaker FreeOffice 2018 è una suite per ufficio veloce, affidabile e compatibile</it>
   <ja>SoftMaker FreeOffice 2018 は、高速で互換性があり、信頼性の高いオフィススイートです</ja>
   <kk>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</kk>
   <ko>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</ko>
   <ku>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</ku>
   <lt>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</lt>
   <mk>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</mk>
   <mr>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</mr>
   <nb_NO>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</nb_NO>
   <nb>SoftMaker FreeOffice 2018 er en rask, kompatibel og pålitelig kontorpakke</nb>
   <nl_BE>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</nl_BE>
   <nl>SoftMaker FreeOffice 2018 is een snel, geschikt en betrouwbaar officeprogramma</nl>
   <or>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</or>
   <pl>SoftMaker FreeOffice 2018 to szybki, kompatybilny i niezawodny pakiet biurowy</pl>
   <pt_BR>O SoftMaker FreeOffice 2018 é uma suite de escritório rápida, compatível e confiável</pt_BR>
   <pt>O SoftMaker FreeOffice 2018 é uma suite de escritório rápida, compatível com outras e fiável</pt>
   <ro>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</ro>
   <ru>SoftMaker FreeOffice 2018 - быстрый, совместимый и надежный офисный пакет</ru>
   <sk>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</sk>
   <sl>SoftMaker FreeOffice 2018 je hiter in združljiv paket za pisarniško rabo</sl>
   <so>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</so>
   <sq>SoftMaker FreeOffice 2018 është një suitë zyrash e shpejtë, e përputhshme dhe e qëndrueshme</sq>
   <sr>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</sr>
   <sv>SoftMaker FreeOffice 2018 är snabb, kompatibel och pålitlig kontors-uppsättning</sv>
   <th>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</th>
   <tr>SoftMaker FreeOffice 2018 hızlı, uyumlu ve güvenilir bir ofis takımıdır</tr>
   <uk>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</uk>
   <vi>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</vi>
   <zh_CN>SoftMaker FreeOffice 2018 是一份快速、兼容、可靠的办公套件</zh_CN>
   <zh_HK>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</zh_HK>
   <zh_TW>SoftMaker FreeOffice 2018 is a fast, compatible and reliable office suite</zh_TW>
</description>

<installable>
32
</installable>

<screenshot>none</screenshot>

<preinstall>

echo "---------------------"
echo "FreeOffice Installer "
echo "---------------------"

# softmaker repo-key
KEY="
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1

mQENBFpOLj4BCAC8XZffd/y5zonHkCFswGagKUO4dYo0VTpHouHo3aShVpw4J/Xh
EHzmMuFN7gAt1wa/mnp8k3gZkj1SvcY/nnFxGE/8SL6nuUzZY8yUbuTP8xkp31U+
kQUpBCefip2ZHdFg3bYMGkWDDVm0cMo7nD1rznNN8GSC8UU8Y6YFrodUc/WIpgEo
GqbS3opFgwMBdrt/+wWTz4/+N5tC4HQXG2IMoC4fodydQu+6iN+cgZVibpJeRybW
rQkmMJAUydBy+o3CRhIE8yFnaJ4mWZieZQy78sWw5bZPFHjg3kOEIxSncFaYHfWY
otRGbQlenA4ij9pqyAJtqBT4LqBET2FHqcmJABEBAAG0RVNvZnRNYWtlciByZXBv
c2l0b3J5IChHUEcga2V5IGZvciBzaWduaW5nIGZpbGVzKSA8aW5mb0Bzb2Z0bWFr
ZXIuY29tPokBOAQTAQIAIgUCWk4uPgIbAwYLCQgHAwIGFQgCCQoLBBYCAwECHgEC
F4AACgkQNBPamKo+f14imQf+PwJOZwTS+3zVQRjBgPjtxSdsOcONnjNhvYoe3N+v
NQZZMOlksndviM7AMB7kcYV5NWiawYvvbg7knpsMdgMzvToB0CpKQ/K7oy8kAq0O
HdtA1HwV/23ExH+EcAtCZnzD8YopRXlcoN6hGG4GkDzSf/Rnj4b6ImtKVBcy0R43
BbbL6cMFJj3Gw51MoxR9ZXBV4job+9T3pt7rCb1mnq4x8ocCLvtT7vgs0QnwC+Pb
PgHXHHTYKcFeoZf3IrGx9ZcMKCbShC0LQv5Kn8PiQZXgIf24RQvp4ib1XO1lY38W
3q4VvxvJIxkmbjGnADwUaESz/hP8I0j5OIVM5Uefb/k0oQ==
=YG9D
-----END PGP PUBLIC KEY BLOCK-----
"
echo "adding Softmaker repository key: 0x3413DA98AA3E7F5E"
echo "$KEY" | gpg --keyid-format="0xlong"  2>/dev/null

# curl -RLJ https://shop.softmaker.com/repo/linux-repo-public.key | apt-key add -
# echo "$KEY" | ( apt-key add - ) 2>/dev/null

rm /etc/apt/trusted.gpg.d/softmaker-keyring.gpg 2>/dev/null
echo "$KEY" | sudo gpg -o /etc/apt/trusted.gpg.d/softmaker-keyring.gpg --dearmor 2>/dev/null

echo "adding Softmaker repository  /etc/apt/sources.list.d/softmaker.list:"
echo "deb http://shop.softmaker.com/repo/apt stable non-free"

rm /etc/apt/sources.list.d/softmaker.list 2>/dev/null
echo "deb http://shop.softmaker.com/repo/apt stable non-free" > /etc/apt/sources.list.d/softmaker.list

apt-get update

</preinstall>

<install_package_names>
softmaker-freeoffice-2018
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
softmaker-freeoffice-2018
</uninstall_package_names>

<postuninstall>

echo "removing Softmaker repository key"
apt-key del 0x3413DA98AA3E7F5E

echo "removing Softmaker repository  /etc/apt/sources.list.d/softmaker.list"
rm /etc/apt/sources.list.d/softmaker.list
echo "DONE"

</postuninstall>

</app>

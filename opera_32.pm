<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Browser
</category>

<name>
Opera
</name>

<description>
   <am>Opera browser (32 bit is beta-only)</am>
   <ar>Opera browser (32 bit is beta-only)</ar>
   <be>Opera browser (32 bit is beta-only)</be>
   <bg>Opera browser (32 bit is beta-only)</bg>
   <bn>Opera browser (32 bit is beta-only)</bn>
   <ca>Navegador Opera (32 bit només beta)</ca>
   <cs>Opera browser (32 bit is beta-only)</cs>
   <da>Opera-browser (32 bit er kun beta)</da>
   <de>Opera-Browser (32 Bit ist nur Beta-Version)</de>
   <el>Πρόγραμμα περιήγησης Opera (32 bit είναι μόνο beta)</el>
   <en>Opera browser (32 bit is beta-only)</en>
   <es_ES>Navegador Opera (32 bit - solo versión beta)</es_ES>
   <es>Navegador Opera (32 bit - solo versión beta)</es>
   <et>Opera browser (32 bit is beta-only)</et>
   <eu>Opera browser (32 bit is beta-only)</eu>
   <fa>Opera browser (32 bit is beta-only)</fa>
   <fil_PH>Opera browser (32 bit is beta-only)</fil_PH>
   <fi>Opera-selain (32-bittinen on vain beta-vaiheessa)</fi>
   <fr_BE>Navigateur Opera (32 bit en beta uniquement)</fr_BE>
   <fr>Navigateur Opera (32 bit en beta uniquement)</fr>
   <gl_ES>Navegador web Opera (32 bit is beta-only)</gl_ES>
   <gu>Opera browser (32 bit is beta-only)</gu>
   <he_IL>Opera browser (32 bit is beta-only)</he_IL>
   <hi>ओपेरा ब्राउज़र (32 बिट केवल बीटा है)</hi>
   <hr>Opera browser (32 bit is beta-only)</hr>
   <hu>Opera browser (32 bit is beta-only)</hu>
   <id>Opera browser (32 bit is beta-only)</id>
   <is>Opera browser (32 bit is beta-only)</is>
   <it>Opera browser (a 32 bit è esclusivamente-beta)</it>
   <ja>Opera ブラウザー (32 bit 版は beta のみ)</ja>
   <kk>Opera browser (32 bit is beta-only)</kk>
   <ko>Opera browser (32 bit is beta-only)</ko>
   <ku>Opera browser (32 bit is beta-only)</ku>
   <lt>Opera naršyklė (32 bitų yra tik beta versija)</lt>
   <mk>Opera browser (32 bit is beta-only)</mk>
   <mr>Opera browser (32 bit is beta-only)</mr>
   <nb_NO>Opera browser (32 bit is beta-only)</nb_NO>
   <nb>Nettleseren Opera (32 bit er kun beta)</nb>
   <nl_BE>Opera browser (32 bit is beta-only)</nl_BE>
   <nl>Opera browser (32 bit is enkel-beta)</nl>
   <or>Opera browser (32 bit is beta-only)</or>
   <pl>przeglądarka Opera (wersja 32-bitowa tylko w wersji beta)</pl>
   <pt_BR>Opera - Versão beta de 32 bits do navegador de internet</pt_BR>
   <pt>Navegador web Opera (32 bit is beta-only)</pt>
   <ro>Opera browser (32 bit is beta-only)</ro>
   <ru>Браузер Opera (32 bit бета-версия)</ru>
   <sk>Opera browser (32 bit is beta-only)</sk>
   <sl>Opera brskalnik (32 bitni je zgolj beta)</sl>
   <so>Opera browser (32 bit is beta-only)</so>
   <sq>Shfletuesi Opera (32 bitshi është vetëm version beta)</sq>
   <sr>Opera browser (32 bit is beta-only)</sr>
   <sv>Opera webbläsare (32 bit är enbart i beta)</sv>
   <th>เบราว์เซอร์ Opera (32 bit เป็นเบต้าเท่านั้น)</th>
   <tr>Opera tarayıcı (32 bit yalnızca betadır)</tr>
   <uk>Браузер Opera (32 біт лише у бета-версії)</uk>
   <vi>Opera browser (32 bit is beta-only)</vi>
   <zh_CN>Opera browser (32 bit is beta-only)</zh_CN>
   <zh_HK>Opera browser (32 bit is beta-only)</zh_HK>
   <zh_TW>Opera browser (32 bit is beta-only)</zh_TW>
</description>

<installable>
32
</installable>

<screenshot>http://www-static.operacdn.com/static-heap/e3/e3be37900045a27590062000a1e380e005644b8a/linux.png</screenshot>

<preinstall>
sed -i -r '/opera.com/ s/^#+//' /etc/apt/sources.list.d/various.list
wget -O - http://deb.opera.com/archive.key | tee /etc/apt/trusted.gpg.d/opera.asc
apt-get update
</preinstall>

<install_package_names>
opera-beta
</install_package_names>


<postinstall>
if [ -f /etc/apt/sources.list.d/opera-stable.list ]; then sed -i -r '/opera.com/ s/^([^#])/#\1/' /etc/apt/sources.list.d/various.list;apt-get update; fi
</postinstall>


<uninstall_package_names>
opera-beta
</uninstall_package_names>
</app>

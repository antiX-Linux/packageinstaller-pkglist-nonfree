<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Misc
</category>

<name>
Google Earth Pro
</name>

<description>
   <am>Google Earth Globe</am>
   <ar>Google Earth Globe</ar>
   <be>Google Earth Globe</be>
   <bg>Google Earth Globe</bg>
   <bn>Google Earth Globe</bn>
   <ca>Mapamundi de Google Earth</ca>
   <cs>Google Earth Globe</cs>
   <da>Google Earth-globus</da>
   <de>Google Earth Globe</de>
   <el>Google Earth Globe</el>
   <en>Google Earth Globe</en>
   <es_ES>Globo Terráqueo de Google Earth</es_ES>
   <es>Globo Terráqueo de Google Earth</es>
   <et>Google Earth Globe</et>
   <eu>Google Earth Globe</eu>
   <fa>Google Earth Globe</fa>
   <fil_PH>Google Earth Globe</fil_PH>
   <fi>Google Earth Maapallo</fi>
   <fr_BE>Google Earth Globe</fr_BE>
   <fr>Google Earth Globe</fr>
   <gl_ES>Google Earth Globo</gl_ES>
   <gu>Google Earth Globe</gu>
   <he_IL>Google Earth Globe</he_IL>
   <hi>गूगल पृथ्वी ग्लोब</hi>
   <hr>Google Earth Globe</hr>
   <hu>Google Earth Globe</hu>
   <id>Google Earth Globe</id>
   <is>Google Earth Globe</is>
   <it>Google Earth Globe</it>
   <ja>Google Earth 地球儀</ja>
   <kk>Google Earth Globe</kk>
   <ko>Google Earth Globe</ko>
   <ku>Google Earth Globe</ku>
   <lt>Google Earth Globe</lt>
   <mk>Google Earth Globe</mk>
   <mr>Google Earth Globe</mr>
   <nb_NO>Google Earth Globe</nb_NO>
   <nb>Globusen Google Earth</nb>
   <nl_BE>Google Earth Globe</nl_BE>
   <nl>Google Earth Wereldbol</nl>
   <or>Google Earth Globe</or>
   <pl>Globus Google Earth</pl>
   <pt_BR>Google Earth - Visualizar o planeta Terra a partir do espaço</pt_BR>
   <pt>Ver a Terra a partir do espaço</pt>
   <ro>Google Earth Globe</ro>
   <ru>Google Планета Земля - спутниковые снимки земного шара</ru>
   <sk>Google Earth Globe</sk>
   <sl>Googlov globus Zemlje</sl>
   <so>Google Earth Globe</so>
   <sq>Google Earth Globe</sq>
   <sr>Google Earth Globe</sr>
   <sv>Google Earth Glob</sv>
   <th>ลูกโลก Google Earth</th>
   <tr>Google Earth Globe</tr>
   <uk>Google Earth Globe</uk>
   <vi>Google Earth Globe</vi>
   <zh_CN>Google 地球</zh_CN>
   <zh_HK>Google Earth Globe</zh_HK>
   <zh_TW>Google Earth Globe</zh_TW>
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
curl https://dl.google.com/linux/linux_signing_key.pub |tee /etc/apt/trusted.gpg.d/google-earth.asc
echo "deb [arch=amd64] http://dl.google.com/linux/earth/deb/ stable main">/etc/apt/sources.list.d/mxpitemp.list
apt-get update
</preinstall>

<install_package_names>
google-earth-pro-stable
</install_package_names>


<postinstall>
rm /etc/apt/sources.list.d/mxpitemp.list
</postinstall>


<uninstall_package_names>
google-earth-pro-stable
</uninstall_package_names>
</app>

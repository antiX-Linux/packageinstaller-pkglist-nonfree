<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Non-Free
</category>

<name>
Non-Free Video codecs
</name>

<description>
   <am>Video codecs and libdvdcss2 from deb-multimedia repos</am>
   <ar>Video codecs and libdvdcss2 from deb-multimedia repos</ar>
   <be>Video codecs and libdvdcss2 from deb-multimedia repos</be>
   <bg>Video codecs and libdvdcss2 from deb-multimedia repos</bg>
   <bn>Video codecs and libdvdcss2 from deb-multimedia repos</bn>
   <ca>Còdecs de vídeo i libdvdcss2 dels dipòsits deb-multimedia</ca>
   <cs>Video codecs and libdvdcss2 from deb-multimedia repos</cs>
   <da>Video codecs and libdvdcss2 from deb-multimedia repos</da>
   <de>Videocodec-Sammlung und Programmbibliothek “libdvdcss2” aus dem Debian Multimedia Repository</de>
   <el>Κωδικοποιητές βίντεο και libdvdcss2 από repos πολυμέσων</el>
   <en>Video codecs and libdvdcss2 from deb-multimedia repos</en>
   <es_ES>Códecs de video y libdvdcss2 de repositorios deb-multimedia</es_ES>
   <es>Códecs de video y libdvdcss2 de repositorios deb-multimedia</es>
   <et>Video codecs and libdvdcss2 from deb-multimedia repos</et>
   <eu>Video codecs and libdvdcss2 from deb-multimedia repos</eu>
   <fa>Video codecs and libdvdcss2 from deb-multimedia repos</fa>
   <fil_PH>Video codecs and libdvdcss2 from deb-multimedia repos</fil_PH>
   <fi>Video codecs and libdvdcss2 from deb-multimedia repos</fi>
   <fr_BE>Codecs vidéo et libdvdcss2 dans les dépôts deb-multimedia</fr_BE>
   <fr>Codecs vidéo et libdvdcss2 dans les dépôts deb-multimedia</fr>
   <gl_ES>Video codecs and libdvdcss2 from deb-multimedia repos</gl_ES>
   <gu>Video codecs and libdvdcss2 from deb-multimedia repos</gu>
   <he_IL>Video codecs and libdvdcss2 from deb-multimedia repos</he_IL>
   <hi>डेबियन मल्टीमीडिया पैकेज-संग्रह से वीडियो कोडेक्स व libdvdcss2</hi>
   <hr>Video codecs and libdvdcss2 from deb-multimedia repos</hr>
   <hu>Video codecs and libdvdcss2 from deb-multimedia repos</hu>
   <id>Video codecs and libdvdcss2 from deb-multimedia repos</id>
   <is>Video codecs and libdvdcss2 from deb-multimedia repos</is>
   <it>Codec video e libdvdcss2 dai repository deb-multimedia</it>
   <ja>ビデオコーデックと libdvdcss2 は deb-multimedia レポより</ja>
   <kk>Video codecs and libdvdcss2 from deb-multimedia repos</kk>
   <ko>Video codecs and libdvdcss2 from deb-multimedia repos</ko>
   <ku>Video codecs and libdvdcss2 from deb-multimedia repos</ku>
   <lt>Video codecs and libdvdcss2 from deb-multimedia repos</lt>
   <mk>Video codecs and libdvdcss2 from deb-multimedia repos</mk>
   <mr>Video codecs and libdvdcss2 from deb-multimedia repos</mr>
   <nb_NO>Video codecs and libdvdcss2 from deb-multimedia repos</nb_NO>
   <nb>Videokodeker og libdvdcss2 fra kodelageret «deb-multimedia»</nb>
   <nl_BE>Video codecs and libdvdcss2 from deb-multimedia repos</nl_BE>
   <nl>Video codecs and libdvdcss2 from deb-multimedia repos</nl>
   <or>Video codecs and libdvdcss2 from deb-multimedia repos</or>
   <pl>Video codecs and libdvdcss2 from deb-multimedia repos</pl>
   <pt_BR>Codecs de vídeo e libdvdcss2 dos repositórios do deb-multimídia</pt_BR>
   <pt>Codecs de video e libdvdcss2dos repositórios deb-multimedia</pt>
   <ro>Video codecs and libdvdcss2 from deb-multimedia repos</ro>
   <ru>Video codecs and libdvdcss2 from deb-multimedia repos</ru>
   <sk>Video codecs and libdvdcss2 from deb-multimedia repos</sk>
   <sl>Video kodeki in libdvdcss2 iz deb-multimedia skladišč</sl>
   <so>Video codecs and libdvdcss2 from deb-multimedia repos</so>
   <sq>Kodekë video dhe libdvdcss2 nga depot deb-multimedia</sq>
   <sr>Video codecs and libdvdcss2 from deb-multimedia repos</sr>
   <sv>Video codecs och libdvdcss2 från deb-multimedia förråd</sv>
   <th>Video codecs and libdvdcss2 from deb-multimedia repos</th>
   <tr>Debian çokluortam depolarından  libdvdcss2 ve görüntü çözücüler</tr>
   <uk>Video codecs and libdvdcss2 from deb-multimedia repos</uk>
   <vi>Video codecs and libdvdcss2 from deb-multimedia repos</vi>
   <zh_CN>Video codecs and libdvdcss2 from deb-multimedia repos</zh_CN>
   <zh_HK>Video codecs and libdvdcss2 from deb-multimedia repos</zh_HK>
   <zh_TW>Video codecs and libdvdcss2 from deb-multimedia repos</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "Adding deb-multimedia repository to /etc/apt/sources.list.d/debian.list"
sed -i -r '/http:.*multimedia\.org.* bookworm/ s/^#+//' /etc/apt/sources.list.d/debian.list
apt-get update
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
libdvdcss2
w64codecs
</install_package_names>

<postinstall>
echo "---- in postprocessing ----"
echo "Removing deb-multimedia repository from /etc/apt/sources.list.d/debian.list"
sed -i -r '/http:.*multimedia\.org.* bookworm/ s/^([^#])/#\1/' /etc/apt/sources.list.d/debian.list
echo "---- postprocessing done----"
</postinstall>

<uninstall_package_names>
libdvdcss2
w64codecs
</uninstall_package_names>

</app>

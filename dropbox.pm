<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Network
</category>

<name>
Dropbox
</name>

<description>
   <am>Dropbox file sync utility</am>
   <ar>Dropbox file sync utility</ar>
   <be>Dropbox file sync utility</be>
   <bg>Dropbox file sync utility</bg>
   <bn>Dropbox file sync utility</bn>
   <ca>Utilitat de sincronització de fitxers amb Dropbox</ca>
   <cs>Dropbox file sync utility</cs>
   <da>Dropbox-filsynkroniseringsredskab</da>
   <de>Dropbox Dateisynchronisationsprogramm</de>
   <el>Βοηθητικό πρόγραμμα συγχρονισμού αρχείων Dropbox</el>
   <en>Dropbox file sync utility</en>
   <es_ES>Sincronizar archivos con Dropbox</es_ES>
   <es>Sincronizar archivos con Dropbox</es>
   <et>Dropbox file sync utility</et>
   <eu>Dropbox file sync utility</eu>
   <fa>Dropbox file sync utility</fa>
   <fil_PH>Dropbox file sync utility</fil_PH>
   <fi>Dropbox on tiedostosynkronointiin tarkoitettu työkalu</fi>
   <fr_BE>Utilitaire de synchronisation de fichier Dropbox</fr_BE>
   <fr>Utilitaire de synchronisation de fichier Dropbox</fr>
   <gl_ES>Utilidade para alamcenamento e sincronización de ficheiros</gl_ES>
   <gu>Dropbox file sync utility</gu>
   <he_IL>Dropbox file sync utility</he_IL>
   <hi>ड्रॉपबॉक्स फाइल समकालीन साधन</hi>
   <hr>Dropbox file sync utility</hr>
   <hu>Dropbox file sync utility</hu>
   <id>Dropbox file sync utility</id>
   <is>Dropbox file sync utility</is>
   <it>Utility di sincronizzazione dei file</it>
   <ja>Dropbox ファイル同期ユーティリティ</ja>
   <kk>Dropbox file sync utility</kk>
   <ko>Dropbox file sync utility</ko>
   <ku>Dropbox file sync utility</ku>
   <lt>Dropbox failų sinchronizavimo paslaugų programa</lt>
   <mk>Dropbox file sync utility</mk>
   <mr>Dropbox file sync utility</mr>
   <nb_NO>Dropbox file sync utility</nb_NO>
   <nb>Filsynkroniseringsverktøyet Dropbox</nb>
   <nl_BE>Dropbox file sync utility</nl_BE>
   <nl>Dropbox bestand synchronisatie gereedschap</nl>
   <or>Dropbox file sync utility</or>
   <pl>narzędzie synchronizacji plików Dropbox</pl>
   <pt_BR>Dropbox - Utilitário de armazenamento e sincronização de arquivos</pt_BR>
   <pt>Utilitário de armazenagem e sincronização de ficheiros</pt>
   <ro>Dropbox file sync utility</ro>
   <ru>Утилита синхронизации файлов с Dropbox</ru>
   <sk>Dropbox file sync utility</sk>
   <sl>orodje za sinhronizacijo Dropbox datotek</sl>
   <so>Dropbox file sync utility</so>
   <sq>Mjet njëkohësimi kartelash për Dropbox</sq>
   <sr>Dropbox file sync utility</sr>
   <sv>Dropbox filsynkroniserings-redskap</sv>
   <th>เครื่องมือซิงค์ไฟล์ Dropbox </th>
   <tr>Dropbox dosya eşzamanlama yardımcısı</tr>
   <uk>утиліта синхронізації файлів Dropbox</uk>
   <vi>ứng dụng đồng bộ hóa tệp Dropbox</vi>
   <zh_CN>Dropbox 文件同步实用工具</zh_CN>
   <zh_HK>Dropbox file sync utility</zh_HK>
   <zh_TW>Dropbox file sync utility</zh_TW>
</description>

<installable>
32,64
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
nautilus-dropbox
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
nautilus-dropbox
</uninstall_package_names>
</app>

<?xml version="1.0" encoding="UTF-8"?>
<app>

<category>
Utility
</category>

<name>
iDevice Mounter
</name>

<description>
   <am>Mount accessible parts of an iPhone's or iPad's filesystem</am>
   <ar>Mount accessible parts of an iPhone's or iPad's filesystem</ar>
   <be>Mount accessible parts of an iPhone's or iPad's filesystem</be>
   <bg>Mount accessible parts of an iPhone's or iPad's filesystem</bg>
   <bn>Mount accessible parts of an iPhone's or iPad's filesystem</bn>
   <ca>Munta les parts accessibles del sistema de fitxers d'iPads o iPhones</ca>
   <cs>Mount accessible parts of an iPhone's or iPad's filesystem</cs>
   <da>Mount accessible parts of an iPhone's or iPad's filesystem</da>
   <de>Zugängliche Bereiche der Dateisysteme von iPhonen oder iPad-Geräten einhängen.</de>
   <el>Τοποθετήστε προσβάσιμα μέρη του συστήματος αρχείων iPhone ή iPad</el>
   <en>Mount accessible parts of an iPhone's or iPad's filesystem</en>
   <es_ES>Montar partes accesibles del sistema de archivos de un iPhone o iPad</es_ES>
   <es>Montar partes accesibles del sistema de archivos de un iPhone o iPad</es>
   <et>Mount accessible parts of an iPhone's or iPad's filesystem</et>
   <eu>Mount accessible parts of an iPhone's or iPad's filesystem</eu>
   <fa>Mount accessible parts of an iPhone's or iPad's filesystem</fa>
   <fil_PH>Mount accessible parts of an iPhone's or iPad's filesystem</fil_PH>
   <fi>Liitä iPhonen tai iPadin tiedostojärjestemät joihin on pääsyoikeus</fi>
   <fr_BE>Monter les parties accessibles du système de fichiers d'un iPhone ou d'un iPad</fr_BE>
   <fr>Monter les parties accessibles du système de fichiers d'un iPhone ou d'un iPad</fr>
   <gl_ES>Mount accessible parts of an iPhone's or iPad's filesystem</gl_ES>
   <gu>Mount accessible parts of an iPhone's or iPad's filesystem</gu>
   <he_IL>Mount accessible parts of an iPhone's or iPad's filesystem</he_IL>
   <hi>आईफोन या आईपैड फाइल सिस्टम के अभिगम योग्य भागों को माउंट करना</hi>
   <hr>Mount accessible parts of an iPhone's or iPad's filesystem</hr>
   <hu>Mount accessible parts of an iPhone's or iPad's filesystem</hu>
   <id>Mount accessible parts of an iPhone's or iPad's filesystem</id>
   <is>Mount accessible parts of an iPhone's or iPad's filesystem</is>
   <it>Monta parti accessibili del filesystem di un iPhone o iPad</it>
   <ja>iPhone/iPad ファイルシステムのアクセス可能な部分をマウントする</ja>
   <kk>Mount accessible parts of an iPhone's or iPad's filesystem</kk>
   <ko>Mount accessible parts of an iPhone's or iPad's filesystem</ko>
   <ku>Mount accessible parts of an iPhone's or iPad's filesystem</ku>
   <lt>Mount accessible parts of an iPhone's or iPad's filesystem</lt>
   <mk>Mount accessible parts of an iPhone's or iPad's filesystem</mk>
   <mr>Mount accessible parts of an iPhone's or iPad's filesystem</mr>
   <nb_NO>Mount accessible parts of an iPhone's or iPad's filesystem</nb_NO>
   <nb>Monter en iPhones eller iPads tilgjengelige filsystem</nb>
   <nl_BE>Mount accessible parts of an iPhone's or iPad's filesystem</nl_BE>
   <nl>Mount accessible parts of an iPhone's or iPad's filesystem</nl>
   <or>Mount accessible parts of an iPhone's or iPad's filesystem</or>
   <pl>Mount accessible parts of an iPhone's or iPad's filesystem</pl>
   <pt_BR>Monte partes acessíveis do sistema de arquivos de um iPhone ou iPad</pt_BR>
   <pt>"Montar"(para conseguir aceder) as partes acessíveis dos sistemas de ficheiros de Iphones e Ipads</pt>
   <ro>Mount accessible parts of an iPhone's or iPad's filesystem</ro>
   <ru>Mount accessible parts of an iPhone's or iPad's filesystem</ru>
   <sk>Mount accessible parts of an iPhone's or iPad's filesystem</sk>
   <sl>Priklop dostopnih delov datotečnega sistema za iPhone ali iPad</sl>
   <so>Mount accessible parts of an iPhone's or iPad's filesystem</so>
   <sq>Montoni pjesë të përdorshme të një sistemi kartelash iPhone ose iPad</sq>
   <sr>Mount accessible parts of an iPhone's or iPad's filesystem</sr>
   <sv>Montera tillgängliga delar av en iPhone's eller iPads filsystem</sv>
   <th>Mount accessible parts of an iPhone's or iPad's filesystem</th>
   <tr>Bir iPhone'un veya iPad'in dosya sisteminin erişilebilir bölümlerini bağlayın</tr>
   <uk>Mount accessible parts of an iPhone's or iPad's filesystem</uk>
   <vi>Mount accessible parts of an iPhone's or iPad's filesystem</vi>
   <zh_CN>Mount accessible parts of an iPhone's or iPad's filesystem</zh_CN>
   <zh_HK>Mount accessible parts of an iPhone's or iPad's filesystem</zh_HK>
   <zh_TW>Mount accessible parts of an iPhone's or iPad's filesystem</zh_TW>
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
idevice-mounter
iphone-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
idevice-mounter
iphone-antix
</uninstall_package_names>
</app>
